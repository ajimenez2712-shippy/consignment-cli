FROM golang:1.10.0 as builder

WORKDIR /go/src/gitlab.com/ajimenez2712-shippy/consignment-cli
COPY . .

RUN go get
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo


FROM alpine:latest

RUN apk --no-cache add ca-certificates

RUN mkdir /app
WORKDIR /app
ADD consignment.json /app/consignment.json
COPY --from=builder /go/src/gitlab.com/ajimenez2712-shippy/consignment-cli/consignment-cli .

CMD ["./consignment-cli"]
