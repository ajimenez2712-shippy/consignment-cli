build:
	docker build -t shippy_consignment-cli .

run:
	docker run \
		-e MICRO_REGISTRY=mdns \
		shippy_consignment-cli
